//
//  FavoriteMovie.swift
//  MovieApp
//
//  Created by Bryan Barreto on 06/02/21.
//

import Foundation
import RealmSwift

class FavoriteMovie: Object {
    @objc dynamic var id: Int = 0
    @objc dynamic var backdrop_path: String = ""
    @objc dynamic var release_date: String = ""
    @objc dynamic var overview: String = ""
    @objc dynamic var title: String = ""
    @objc dynamic var poster_path: String = ""
    
    func setup(id: Int, backdrop: String, date: String, overview: String, title: String, poster: String) {
        self.id = id
        self.backdrop_path = backdrop
        self.release_date = date
        self.overview = overview
        self.title = title
        self.poster_path = poster
    }
}
