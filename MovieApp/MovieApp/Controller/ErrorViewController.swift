//
//  ErrorViewController.swift
//  MovieApp
//
//  Created by Bryan Barreto on 04/02/21.
//

import UIKit

protocol ErrorViewControllerDelegate {
    func tryAgain()
}

class ErrorViewController: UIViewController {

    @IBOutlet weak var tryAgain: UIButton!
    
    var delegate: ErrorViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tryAgain.layer.cornerRadius = 25
        self.tryAgain.backgroundColor = .darkGray
        self.tryAgain.tintColor = .white
        self.navigationItem.hidesBackButton = true
    }
    
    @IBAction func tryAgainTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
        self.delegate?.tryAgain()
    }
}
