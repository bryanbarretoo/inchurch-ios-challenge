//
//  MovieRequest.swift
//  MovieApp
//
//  Created by Bryan Barreto on 03/02/21.
//

import Foundation

struct MovieRequest {
    
    private var request: URLRequest
    
    private let API_BASE_URL: String = "https://api.themoviedb.org/3"
    private let API_IMAGE_BASE_URL: String = "https://image.tmdb.org/t/p/w500"
    private let API_KEY: String = "eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIxNzYwMWY2M2RlMzYzYWY2ODU3NzdjNmNkYmI0MjU3NCIsInN1YiI6IjVmNjYxYWNhODQ0NDhlMDAzOTY5Mzk2ZCIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.BzgI0VI40vxdtVjVliKHFvxas5zIO90LRw9_LOz6ets"
    
    let endpoint: String = "/movie/popular"
    
    init(){
        let stringURL: String = self.API_BASE_URL + endpoint
        guard let url: URL = URL(string: stringURL)
        self.request = URLRequest(url: <#T##URL#>)
    }
}
