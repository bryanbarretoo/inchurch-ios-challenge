//
//  Realm.swift
//  MovieApp
//
//  Created by Bryan Barreto on 06/02/21.
//

import Foundation
import RealmSwift

class FavoriteAPI {
    static let favoriteAPI = FavoriteAPI()
    private let realm = try! Realm()
    
    func favorite(_ movie:FavoriteMovie, success: @escaping () -> Void, fail: @escaping (String) -> Void){
        do {
            try self.realm.write {
                self.realm.add(movie)
                success()
            }
        }catch {
            fail("Erro ao favoritar filme")
        }
    }
    
    func unfavorite(_ movie:FavoriteMovie, success: @escaping () -> Void, fail: @escaping (String) -> Void){
        
        let predicate = NSPredicate(format: "title = %@", movie.title)
        
        do {
            try self.realm.write {
                let deleteFavorite = realm.objects(FavoriteMovie.self).filter(predicate)
                self.realm.delete(deleteFavorite)
                success()
            }
        }catch {
            fail("Erro ao desfavoritar filme")
        }
        
    }
    
    func isFavorite(withTitle title: String) -> Bool {
        let predicate = NSPredicate(format: "title = %@", title)
        let favorites = realm.objects(FavoriteMovie.self).filter(predicate)
        
        return favorites.count > 0 ? true : false
    }
    
    func getFavorites(search: String? = nil, success: @escaping (Results<FavoriteMovie>) -> Void, fail: @escaping (String) -> Void){
        do {
            try realm.write {
                var favorites = realm.objects(FavoriteMovie.self)
                
                if let search = search, search.count > 0 {
                    let predicate = NSPredicate(format: "title contains[c] %@", search)
                    favorites = favorites.filter(predicate)
                }
                success(favorites)
            }
        }catch {
            fail(error.localizedDescription)
        }
    }
    
    func deleteFavorite(title: String){
        do {
            try realm.write {
                let predicate = NSPredicate(format: "title = %@", title)
                let favorite = realm.objects(FavoriteMovie.self).filter(predicate)
                realm.delete(favorite)
            }
        }catch {
        
        }
    }
}
