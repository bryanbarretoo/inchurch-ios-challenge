//
//  Genres.swift
//  MovieApp
//
//  Created by Bryan Barreto on 03/02/21.
//

import Foundation


struct Genre: Codable {
    let id: Int
    let name: String
}
