//
//  MainTabbarViewController.swift
//  MovieApp
//
//  Created by Bryan Barreto on 03/02/21.
//

import UIKit

class MainTabbarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureTabbar()
        self.configureViewControllers()
    }

    private func configureTabbar(){
        self.tabBar.isTranslucent = false
        self.tabBar.tintColor = .label
    }
    
    private func configureViewControllers(){
        let movieListNavigation = self.buildNavigationController(rootViewController: MovieListViewController(), tabbarItemImage: "film", selectedImage: "film.fill", title: "Movies")
        
        let favoriteListNavigation = self.buildNavigationController(rootViewController: MovieFavoriteViewController(), tabbarItemImage: "star", selectedImage: "star.fill", title: "Favorites")
        
        self.viewControllers = [movieListNavigation, favoriteListNavigation]
    }
    
    private func buildNavigationController(rootViewController: UIViewController, tabbarItemImage: String, selectedImage: String? = nil, title: String) -> UINavigationController {
        let vc = rootViewController
        vc.view.backgroundColor = .white
        let nav = UINavigationController(rootViewController: vc)
        nav.tabBarItem.image = UIImage(systemName: tabbarItemImage)
        nav.tabBarItem.selectedImage = UIImage(systemName: selectedImage ?? tabbarItemImage)
        nav.tabBarItem.title = title
        nav.navigationBar.isTranslucent = false
        
        return nav
    }
}
