//
//  MovieList.swift
//  MovieApp
//
//  Created by Bryan Barreto on 03/02/21.
//

import Foundation

struct MovieList: Codable {
    let id: Int?
    let poster_path: String?
    let title: String?
}
