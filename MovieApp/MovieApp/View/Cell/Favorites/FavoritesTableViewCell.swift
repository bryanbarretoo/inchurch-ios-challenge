//
//  FavoritesTableViewCell.swift
//  MovieApp
//
//  Created by Bryan Barreto on 06/02/21.
//

import UIKit

class FavoritesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var poster: UIImageView!
    @IBOutlet weak var overview: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var date: UILabel!
    
    static let id = "FavoritesTableViewCell"

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setup(_ movie: Movie){
        self.poster.imageFromURL(movie.poster_path!)
        
        self.title.text = movie.title
        self.overview.text = movie.overview
        self.date.text = self.formatDate(movie.release_date!)
        
    }
    
    private func formatDate(_ date: String) -> String {
        var arr = date.split(separator: "-")
        arr.reverse()
        let formatedDate = arr.joined(separator: "/")
        return formatedDate
    }
    
}
