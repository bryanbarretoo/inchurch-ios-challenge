//
//  MovieListViewController.swift
//  MovieApp
//
//  Created by Bryan Barreto on 03/02/21.
//

import UIKit
import Network

class MovieListViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    private var movies: [MovieList] = []
    private var page: Int = 1
    private var totalPages: Int?
    private var isLoading: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "The Movies"
        self.configureCollectionView()
//        self.getPopularMovies(page: self.page)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getPopularMovies(page: self.page)
    }
    
    private func configureCollectionView(){
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.collectionViewLayout = UICollectionViewFlowLayout()
        
        self.collectionView.register(withCellID: MovieCollectionViewCell.id)
    }
    
    private func getPopularMovies(page: Int){
        if self.isLoading {
            return
        }
        
        if let totalPages = self.totalPages {
            if totalPages == self.page {
                print("Erro, maximo de paginas atingidas")
            }
        }
        
        self.view.showLoading()
        MovieAPI.movieApi.getPopularMovies(page: self.page) { (movieResult) in
            self.movies += movieResult.results
            self.page = movieResult.page + 1
            self.totalPages = movieResult.total_pages
            DispatchQueue.main.async {
                self.collectionView.reloadData()
                self.view.hideLoading()
            }
            self.isLoading = false
            
        } onFail: { (error) in
            self.isLoading = false
            
            DispatchQueue.main.async {
                self.view.hideLoading()
                self.showError()
            }
        }
    }
    
    private func showError(){
        let vc = ErrorViewController()
        vc.modalPresentationStyle = .fullScreen
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
}

extension MovieListViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MovieCollectionViewCell.id, for: indexPath) as? MovieCollectionViewCell
        
        let title = self.movies[indexPath.row].title ?? ""
        
        let isFavorite = FavoriteAPI.favoriteAPI.isFavorite(withTitle: title)
        
        cell?.configure(movie: self.movies[indexPath.row], isFavorite: isFavorite)
        return cell ?? UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if indexPath.row == self.movies.count - 10 {
            self.getPopularMovies(page: self.page)
            self.isLoading = true
        }
        
    }
}

extension MovieListViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let movie = self.movies[indexPath.row]
        
        let vc = MovieDetailViewController(title: movie.title ?? "", id: movie.id ?? 0)
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension MovieListViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = self.view.frame.size.width/2.1
        return CGSize(width: width, height: 300)
    }
}

extension MovieListViewController: ErrorViewControllerDelegate {
    func tryAgain() {
        self.getPopularMovies(page: 1)
    }
}
