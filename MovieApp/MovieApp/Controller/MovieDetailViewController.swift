//
//  MovieDetailViewController.swift
//  MovieApp
//
//  Created by Bryan Barreto on 03/02/21.
//

import UIKit

class MovieDetailViewController: UIViewController {
    
    
    @IBOutlet weak var backdrop: UIImageView!
    @IBOutlet weak var releaseDate: UILabel!
    @IBOutlet weak var genres: UILabel!
    @IBOutlet weak var overview: UILabel!
    @IBOutlet weak var voteAverage: UILabel!
    
    let id: Int
    var movie: Movie?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.getMovieDetail(id: self.id)
    }
    
    init(title: String, id: Int) {
        self.id = id
        super.init(nibName: nil, bundle: nil)
        self.title = title
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configNavigation(){
        
        let barButton = UIBarButtonItem()
        barButton.target = self
        if !FavoriteAPI.favoriteAPI.isFavorite(withTitle: self.movie!.title!) {
            barButton.title = "Favorite 👍"
            barButton.action = #selector(favoriteMovie)
        }else{
            barButton.title = "UnFavorite 👎"
            barButton.action = #selector(unfavoriteMovie)
        }
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    private func getCurrentMovie() -> FavoriteMovie? {
        
        guard let id = self.movie?.id,
              let title = self.movie?.title,
              let backdrop = self.movie?.backdrop_path,
              let date = self.movie?.release_date,
              let poster = self.movie?.poster_path,
              let overview = self.movie?.overview else {
            return nil
        }
        
        let favorite = FavoriteMovie()
        
        favorite.setup(id: id, backdrop: backdrop, date: date, overview: overview, title: title, poster: poster)
        
        return favorite
    }
    
    @objc private func favoriteMovie(){
        
        guard let favorite = self.getCurrentMovie() else {
            self.showAlert(title: "Erro", message: "Não foi possível favoritar este filme", btnAction: nil)
            return
        }
        
        showConfirmAlert(title: "Favoritar", message: "Gostaria de favoritar \(self.movie!.title ?? "ERRO")?", okAction: { _ in
            
            FavoriteAPI.favoriteAPI.favorite(favorite) { [weak self] in
                let alert = UIAlertController(title: "Sucesso", message: "\(favorite.title) foi favoritado! 🎬🍿", preferredStyle: .alert)
                self?.configNavigation()
                self?.present(alert, animated: true, completion: {
                    DispatchQueue.main.asyncAfter(wallDeadline: .now() + 1.5) {
                        alert.dismiss(animated: true, completion: nil)
                    }
                })
            } fail: { [weak self]  (error) in
                self?.showAlert(title: "Erro", message: error)
            }

             
        }, cancelAction: nil)
    }
    
    @objc func unfavoriteMovie(){
        
        guard let favorite = self.getCurrentMovie() else {
            self.showAlert(title: "Erro", message: "Não foi possível desfavoritar este filme", btnAction: nil)
            return
        }
        
        showConfirmAlert(title: "Desavoritar", message: "Gostaria de desfavoritar \(self.movie!.title ?? "ERRO")?", okAction: { _ in
            
            FavoriteAPI.favoriteAPI.unfavorite(favorite) { [weak self] in
                let alert = UIAlertController(title: "Sucesso", message: "\(favorite.title) foi desfavoritado! 🎥🚫", preferredStyle: .alert)
                self?.configNavigation()
                self?.present(alert, animated: true, completion: {
                    DispatchQueue.main.asyncAfter(wallDeadline: .now() + 1.5) {
                        alert.dismiss(animated: true, completion: nil)
                    }
                })
            } fail: { [weak self]  (error) in
                self?.showAlert(title: "Erro", message: error)
            }

             
        }, cancelAction: nil)
    }
    
    func getMovieDetail(id: Int){
        self.view.showLoading()
        MovieAPI.movieApi.getMovieDetail(id: id) { (movie) in
            self.movie = movie
            
            DispatchQueue.main.async {
                self.updateUI()
                self.view.hideLoading()
            }
        } onFail: { (error) in
            self.showAlert(title: "Erro", message: error.rawValue) { [weak self] _ in
                self?.view.hideLoading()
                self?.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    private func updateUI(){
        if let backdrop = self.movie?.backdrop_path {
            self.backdrop.imageFromURL(backdrop)
        }
        
        if let releaseDate = self.movie?.release_date {
            self.releaseDate.text = "Release Date: \(self.formatDate(releaseDate))"
        }
        
        if let genres = self.movie?.genres {
            self.genres.text = self.formatGenres(genres)
        }
        
        if let overview = self.movie?.overview {
            self.overview.text = overview
        }
        
        if let vote = self.movie?.vote_average {
            self.voteAverage.text = "\(vote)⭐️"
        }
        
        self.configNavigation()
    }
    
    private func formatDate(_ date: String) -> String {
        var dateArray = date.split(separator: "-")
        
        dateArray = dateArray.reversed()
        
        let formattedDate = dateArray.joined(separator: "/")
        
        return formattedDate
    }
    
    private func formatGenres(_ genres:[Genre]) -> String {
        let genresDescriptions = genres.map {
            $0.name
        }
        
        return genresDescriptions.joined(separator: ", ")
    }

}
