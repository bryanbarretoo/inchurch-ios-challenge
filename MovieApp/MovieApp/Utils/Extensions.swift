//
//  Extensions.swift
//  MovieApp
//
//  Created by Bryan Barreto on 03/02/21.
//

import UIKit
import Kingfisher


// MARK: - UIImageView
extension UIImageView {
    func imageFromURL(_ url: String){
        let strUrl = "https://image.tmdb.org/t/p/w500" + url
        guard let url = URL(string: strUrl) else {
            self.image = UIImage(systemName: "nosign")
            return
        }
//        self.kf.setImage(with: url)
        self.kf.setImage(with: url, placeholder: UIImage(systemName: "camera"), options: [.keepCurrentImageWhileLoading, .transition(ImageTransition.fade(0.5))], completionHandler: nil)
    }
}


// MARK: - UICollectionView
extension UICollectionView {
    func register(withCellID id: String){
        let nib = UINib(nibName: id, bundle: nil)
        self.register(nib, forCellWithReuseIdentifier: id)
    }
}

// MARK: - UITableView
extension UITableView {
    func register(withCellID id: String){
        let nib = UINib(nibName: id, bundle: nil)
        self.register(nib, forCellReuseIdentifier: id)
    }
}

// MARK: - UIView
extension UIView{

    func showLoading(activityColor: UIColor = .black, backgroundColor: UIColor = UIColor.white.withAlphaComponent(0.8)) {
        let backgroundView = UIView()
        backgroundView.frame = CGRect.init(x: 0, y: 0, width: self.bounds.width, height: self.bounds.height)
        backgroundView.backgroundColor = backgroundColor
        backgroundView.tag = 475647
        
        var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
        activityIndicator = UIActivityIndicatorView(frame: CGRect.init(x: 0, y: 0, width: 50, height: 50))
        activityIndicator.center = self.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = .large
        activityIndicator.color = activityColor
        activityIndicator.startAnimating()
        self.isUserInteractionEnabled = false
        
        backgroundView.addSubview(activityIndicator)

        self.addSubview(backgroundView)
    }

    func hideLoading() {
        if let background = viewWithTag(475647){
            background.removeFromSuperview()
        }
        self.isUserInteractionEnabled = true
    }
}

// MARK: - UIViewController
extension UIViewController {
    func showAlert(title: String?, message: String?, btnAction: ((UIAlertAction) -> Void)? = nil){
        let alert = self.buildAlert(title: title, message: message)
        
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: btnAction)
        alert.addAction(okAction)
        
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func showConfirmAlert(title: String?, message: String?, okTitle: String = "Ok", okAction: ((UIAlertAction) -> Void)?, cancelTitle: String = "Cancelar", cancelAction: ((UIAlertAction) -> Void)?){
        let alert = self.buildAlert(title: title, message: message)
        
        let okAction = UIAlertAction(title: okTitle, style: .default, handler: okAction)
        let cancelAction = UIAlertAction(title: cancelTitle, style: .cancel, handler: cancelAction)
        
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func buildAlert(title: String?, message: String?) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        return alert
    }
}
