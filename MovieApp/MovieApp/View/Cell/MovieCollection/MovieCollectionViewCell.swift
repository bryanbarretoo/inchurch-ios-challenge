//
//  MovieCollectionViewCell.swift
//  MovieApp
//
//  Created by Bryan Barreto on 03/02/21.
//

import UIKit

class MovieCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var poster: UIImageView!
    @IBOutlet weak var favorite: UIImageView!
    
    static let id: String = "MovieCollectionViewCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.favorite.tintColor = UIColor(red: 212/255, green: 175/255, blue: 55/255, alpha: 1)
        self.poster.layer.cornerRadius = 20
    }
    
    func configure(movie: MovieList, isFavorite: Bool){
        
        self.favorite.image = UIImage(systemName: "star")
        
        if let poster = movie.poster_path {
            self.poster.imageFromURL(poster)
        }
        
        if let title = movie.title {
            self.title.text = title
        }
        
        if isFavorite {
            self.favorite.image = UIImage(systemName: "star.fill")
        }
    }
}
