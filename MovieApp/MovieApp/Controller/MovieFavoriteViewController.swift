//
//  MovieFavoriteViewController.swift
//  MovieApp
//
//  Created by Bryan Barreto on 03/02/21.
//

import UIKit
import RealmSwift

class MovieFavoriteViewController: UIViewController {
    
    @IBOutlet weak var searchbar: UISearchBar!
    @IBOutlet weak var tableview: UITableView!
    
//    var favorites: Results<FavoriteMovie>?
    var favorites: [Movie]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Favorites"
        self.tableview.register(withCellID: FavoritesTableViewCell.id)
        self.searchbar.delegate = self
        self.tableview.dataSource = self
        self.tableview.delegate = self

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getFavoriteMovies()
    }
    
    private func getFavoriteMovies(search: String? = nil){
        FavoriteAPI.favoriteAPI.getFavorites(search: search) {(results) in
            
            var movies:[Movie] = []
            for m in results {
                let favorite = Movie(id: m.id, backdrop_path: m.backdrop_path, poster_path: m.poster_path, genres: nil, release_date: m.release_date, overview: m.overview, title: m.title, vote_average: nil)
                movies.append(favorite)
            }
            self.favorites = movies
            self.tableview.reloadData()
        } fail: { (error) in
            self.showAlert(title: "Erro", message: error)
        }

    }
}

extension MovieFavoriteViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let count = self.favorites?.count else {
            return 1
        }
    
        return count > 0 ? count : 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.favorites?.count == 0 {
            let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
            cell.textLabel?.text = "No movie found!"
            return cell
        }
        let cell = tableview.dequeueReusableCell(withIdentifier: FavoritesTableViewCell.id, for: indexPath) as? FavoritesTableViewCell
        
        let movie = self.favorites?[indexPath.row]
        
        cell?.setup(movie!)
        return cell ?? UITableViewCell()
    }
}

extension MovieFavoriteViewController: UITableViewDelegate {
 
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let movie = self.favorites?[indexPath.row]
    
        tableview.deselectRow(at: indexPath, animated: true)
        
        let vc = MovieDetailViewController(title: movie!.title!, id: movie!.id!)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "DELETE"
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let title = self.favorites![indexPath.row].title!
            
            tableview.beginUpdates()
            FavoriteAPI.favoriteAPI.deleteFavorite(title: title)
            tableview.deleteRows(at: [indexPath], with: .automatic)
            self.favorites?.remove(at: indexPath.row)
            tableview.endUpdates()
        }
    }
}


extension MovieFavoriteViewController: UISearchBarDelegate {
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchbar.text = ""
        searchbar.endEditing(true)
        self.getFavoriteMovies()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchbar.showsCancelButton = true
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let searchText = searchbar.text else {
            return
        }
        
        self.getFavoriteMovies(search: searchText)
        searchbar.endEditing(true)
    }

}
