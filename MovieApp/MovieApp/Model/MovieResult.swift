//
//  MovieResult.swift
//  MovieApp
//
//  Created by Bryan Barreto on 03/02/21.
//

import Foundation

struct MovieResult: Codable {
    let page: Int
    let results: [MovieList]
    let total_results: Int
    let total_pages: Int
}
