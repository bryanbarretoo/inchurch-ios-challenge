//
//  MovieAPI.swift
//  MovieApp
//
//  Created by Bryan Barreto on 03/02/21.
//

import Foundation


class MovieAPI {
    
    static let movieApi = MovieAPI()
    
    private let session: URLSession = URLSession.shared
    private let API_BASE_URL: String = "https://api.themoviedb.org/3"
    private let API_IMAGE_BASE_URL: String = "https://image.tmdb.org/t/p/w500"
    private let popularMovieEndpoint: String = "/movie/popular"
    private let API_KEY: String = "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIxNzYwMWY2M2RlMzYzYWY2ODU3NzdjNmNkYmI0MjU3NCIsInN1YiI6IjVmNjYxYWNhODQ0NDhlMDAzOTY5Mzk2ZCIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.BzgI0VI40vxdtVjVliKHFvxas5zIO90LRw9_LOz6ets"
    
    func getPopularMovies(page: Int, onSuccess: @escaping (MovieResult) -> Void, onFail: @escaping (RequestError) -> Void) {
        
        let endpoint = popularMovieEndpoint + "?page=\(page)"
        
        guard let url = self.buildURL(endpoint) else {
            onFail(.badURL)
            return
        }
        
        let req = self.buildRequest(url: url)
        let task = self.session.dataTask(with: req) { (data, response, error) in
            
            if let _ = error {
                onFail(.requestError)
                return
            }
            
            guard let data = data else {
                onFail(.emptyData)
                return
            }
            
            do {
                let object = try JSONDecoder().decode(MovieResult.self, from: data)
                
                guard object.results.count > 0 else {
                    onFail(.emptyData)
                    return
                }
                
                onSuccess(object)
                return
            }catch {
                onFail(.parseObject)
            }
        }
        
        task.resume()
    }
    
    func getMovieDetail(id: Int, onSuccess: @escaping (Movie) -> Void, onFail: @escaping (RequestError) -> Void){
        
        let endpoint = self.movieDetailEndpoint(id: id)
        
        guard let url = self.buildURL(endpoint) else {
            onFail(.badURL)
            return
        }
        
        let req = self.buildRequest(url: url)
        let task = self.session.dataTask(with: req) { (data, response, error) in
            
            if let _ = error {
                onFail(.requestError)
                return
            }
            
            guard let data = data else {
                onFail(.emptyData)
                return
            }
            
            do {
                let object = try JSONDecoder().decode(Movie.self, from: data)
                
                guard (object.id != nil) else {
                    onFail(.emptyData)
                    return
                }
                onSuccess(object)
                return
            }catch {
                onFail(.parseObject)
            }
        }
        task.resume()
    }
    
    private func buildRequest(url: URL) -> URLRequest {
        var req: URLRequest = URLRequest(url: url)
        req.timeoutInterval = 5
        req.setValue(self.API_KEY, forHTTPHeaderField: "Authorization")
        return req
    }
    
    private func movieDetailEndpoint(id: Int) -> String {
        return "/movie/\(id)"
    }
    
    private func buildURL(_ endpoint: String) -> URL? {
        let str = self.API_BASE_URL + endpoint
        guard let url = URL(string: str) else {
            return nil
        }
        return url
    }
}
