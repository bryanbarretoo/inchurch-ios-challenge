//
//  Movie.swift
//  MovieApp
//
//  Created by Bryan Barreto on 03/02/21.
//

import Foundation

struct Movie: Codable {
    let id: Int?
    let backdrop_path: String?
    let poster_path: String?
    let genres: [Genre]?
    let release_date: String?
    let overview: String?
    let title: String?
    let vote_average: Double?
}
