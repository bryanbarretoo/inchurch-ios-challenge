//
//  RequestError.swift
//  MovieApp
//
//  Created by Bryan Barreto on 03/02/21.
//

import Foundation

enum RequestError: String {
    case badURL = "Erro ao montar URL"
    case noInternet = "Dispositivo sem conexão com internet"
    case requestError = "Erro ao realizar requisição"
    case emptyData = "Requisição retornou data vazia"
    case parseObject = "Não foi possível converter objeto"
}
